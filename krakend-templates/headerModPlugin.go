package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
)

func init() {
	fmt.Println("headerModPlugin plugin is loaded!")
}

func main() {}

// HandlerRegisterer is the name of the symbol krakend looks up to try and register plugins
var HandlerRegisterer registrable = registrable("headerModPlugin")

type registrable string

const pluginName = "headerModPlugin"

func (r registrable) RegisterHandlers(f func(
	name string,
	handler func(
		context.Context,
		map[string]interface{},
		http.Handler) (http.Handler, error),
)) {
	f(pluginName, r.registerHandlers)
}

func (r registrable) registerHandlers(ctx context.Context, extra map[string]interface{}, handler http.Handler) (http.Handler, error) {

	client := &http.Client{Timeout: 3 * time.Second}

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		// формируем запрос в сторону s3 сервиса на node.js
		rq, err := http.NewRequest(http.MethodPut, fmt.Sprintf("http://localhost:3000%v", r.URL), nil)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		// копируем все заголовки из оригинального запроса
		rq.Header = r.Header

		// fmt.Printf("%+v", r.Header)

		// и добавляем необходимый заголовок для работы сервиса s3 на node.js
		rq.Header.Set("x-forwarded-host", r.Host)

		rs, err := client.Do(rq)
		if err != nil {
			http.Error(w, err.Error(), http.StatusNotAcceptable)
			return
		}
		defer rs.Body.Close()

		rsBodyBytes, err := ioutil.ReadAll(rs.Body)
		if err != nil {
			http.Error(w, err.Error(), http.StatusNotAcceptable)
			return
		}

		// тут мы получили все данные для нового запроса и можем его выполнить в сторону Селектела

		var myStoredVariable map[string]interface{}

		json.Unmarshal([]byte(rsBodyBytes), &myStoredVariable)

		// fmt.Printf("%+v", myStoredVariable)

		header := myStoredVariable["headers"].(map[string]interface{})
		r2 := new(http.Request)
		*r2 = *r

		// fmt.Printf("%+v", header["authorization"])

		r2.Header.Set("authorization", header["authorization"].(string))
		r2.Header.Set("x-amz-content-sha256", header["x_amz_content_sha256"].(string))
		r2.Header.Set("x-amz-date", header["x_amz_date"].(string))

		// TODO здесь надо будет направить вызов на нужный url

		r2.URL.Path = myStoredVariable["url"].(string)

		// fmt.Printf("%+v", r2.URL.String())

		handler.ServeHTTP(w, r2)
	}), nil
}
