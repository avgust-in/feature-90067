# KrakenD S3

API-gateway для проксирования S3 API

Для валидации запросов к [сервису S3](https://gitlab.timeweb.net/cloud/s3)
реализован кастомный плагин на `Go`.

Пример создания плагина: https://www.eventslooped.com/posts/krakend-writing-plugins/

Версии `Go`, используемые для сборки KrakenD и плагина **обязательно** должны быть одинаковыми,
для версии `krakend 1.4.1` - версия `Go 1.16.5`.

Для сборки плагина из директории `krakend-templates` выполните команды:

```bash
go build -buildmode=plugin -o headerModPlugin.so
```

Собранный плагин будет называться `headerModPlugin.so` и для корректного запуска `krakend`
он должен находиться в одной директории с `krakend.json`.

Для запуска `krakend` можно использовать команду:

```bash
krakend run -c krakend.json
```
