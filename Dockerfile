FROM golang:1.17.3 AS builder
COPY . /app

# собираем бинарник
WORKDIR /app
RUN make build

# собираем плагин
WORKDIR /app/krakend-templates
RUN go build -buildmode=plugin -o headerModPlugin.so


FROM debian:buster-slim

LABEL maintainer="dortiz@devops.faith"

RUN apt-get update && \
	apt-get install -y ca-certificates && \
	update-ca-certificates && \
	rm -rf /var/lib/apt/lists/*

COPY --from=builder /app/krakend /usr/bin/krakend
COPY --from=builder /app/krakend-templates/krakend.json /etc/krakend/krakend.json
COPY --from=builder /app/krakend-templates/headerModPlugin.so /etc/krakend/headerModPlugin.so

RUN useradd -r -c "KrakenD user" -U krakend

USER krakend

VOLUME [ "/etc/krakend" ]

WORKDIR /etc/krakend

EXPOSE 8080

ENTRYPOINT [ "/usr/bin/krakend" ]
CMD [ "run", "-c", "/etc/krakend/krakend.json" ]